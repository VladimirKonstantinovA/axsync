package com.example.axsync;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AxsyncApplication {

    public static void main(String[] args) {
        SpringApplication.run(AxsyncApplication.class, args);
        mainClass.start(args);
    }

}
