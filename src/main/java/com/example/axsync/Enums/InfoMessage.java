package com.example.axsync.Enums;

public enum InfoMessage {
    EMPTY_ARGS("Не указаны параметры!"),
    INCORRECT_COUNT_ARGS("Не указаны параметры!"),
    INVALID_ARGS("Указаны некорректные параметры!"),
    INVALID_TAG_IDENTITY("Указан некорректный файл. Не содержит(или содержит неверный) тэг <Identity>"),
    INVALID_TAG_SEARCHTYPE("Указан некорректный файл. Не содержит(или содержит неверный) тэг <SearchType>"),
    EMPTY_TAG_LOGPATH("Не указан путь как папке логов."),
    EMPTY_TAG_LOGFILENAMEFILTER("Не указан фильтр для имени файла."),
    EMPTY_TAG_SEARCHSTRING("Не указана строка поиска в содержимом файла."),
    EMPTY_TAG_DBDRIVER("Не указан драйвер SQL."),
    EMPTY_TAG_DBPATH("Не указан путь к серверу."),
    EMPTY_TAG_DBNAME("Не указано имя базы данных."),
    EMPTY_TAG_DBUSER("Не указано имя пользователя."),
    EMPTY_TAG_DBPASS("Не указан пароль пользователя."),
    EMPTY_TAG_QUERY("Не указана строка запроса."),
    INVALID_TAG_DBDRIVER("Указан некорректный/неизвестный драйвер сервера базы данных.");

    final String name;

    InfoMessage(String s) {
        name = s;
    }
}
