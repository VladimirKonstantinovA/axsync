package com.example.axsync.Enums;

public enum ReturnCode {
    SUCCESSFUL,
    EMPTY_ARGS,
    INCORRECT_COUNT_ARGS,
    INVALID_ARGS
}
