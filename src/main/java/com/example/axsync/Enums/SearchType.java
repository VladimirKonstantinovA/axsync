package com.example.axsync.Enums;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;

@XmlEnum(String.class)
public enum SearchType {
    @XmlEnumValue("") UNKNOWN,
    @XmlEnumValue("LogSearch") LOGSEARCH,
    @XmlEnumValue("SQLSearch") SQLSEARCH
}
