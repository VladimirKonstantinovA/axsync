package com.example.axsync.Enums;

public enum ContentFileType {
    UNKNOWN,
    TXT,
    XLS,
    XLSX;
}
