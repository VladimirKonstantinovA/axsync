package com.example.axsync.Enums;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;

@XmlEnum(String.class)
public enum Status {
    @XmlEnumValue("OK") OK,
    @XmlEnumValue("Error") Error
}
