package com.example.axsync.LogSearch;

import com.example.axsync.Enums.ContentFileType;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Iterator;

public class FileContentManager {

    private static ContentFileType getContentType(File file) {
        String fileName = file.getName();
        if (! (fileName.lastIndexOf(".") > 0)) {
            return ContentFileType.UNKNOWN;
        }
        String extension = fileName.substring(fileName.lastIndexOf(".")+1).toUpperCase();
        try {
            ContentFileType contentFileType = ContentFileType.valueOf(extension);
            return contentFileType;
        } catch (IllegalArgumentException ex) {
            ContentFileType contentFileType = ContentFileType.UNKNOWN;
            return contentFileType;
        }
    }

    public static Boolean fileWithFilters(File file, ArrayList<String> filters) {
        ContentFileType contentFileType = getContentType(file);
        Boolean result;
            switch (contentFileType) {
                case TXT:
                    result = searchInTEXT(file, filters);
                    break;
                case XLS:
                case XLSX:
                    result = searchInEXCEL(file, filters);
                    break;
                default:
                    result = false;
            }
        return result;
    }

    private static Boolean searchInTEXT(File file, ArrayList<String> filters) {
        Path path = Path.of(file.getPath());
        Boolean haveFilters;
        try {
        haveFilters = Files.lines(path).anyMatch(line->(filters.stream().anyMatch(filter->line.toUpperCase().contains(filter))));
        }
        catch (IOException ex) {
            return false;
        }
        return haveFilters;
    }

    private static Boolean searchInEXCEL(File file, ArrayList<String> filters) {
        String path = file.getPath();
        Workbook workbook;
        try {
            if (path.substring(path.lastIndexOf(".")+1).toUpperCase().contains("XLSX")) {
                workbook = new XSSFWorkbook(path);
            } else {
                workbook = new HSSFWorkbook(new FileInputStream(path));
            }
            ;
            Iterator<Sheet> sheetIterator = workbook.sheetIterator();
            while (sheetIterator.hasNext()) {
                Sheet sheet = sheetIterator.next();
                Iterator<Row> rowIterator = sheet.rowIterator();
                while (rowIterator.hasNext()) {
                    Iterator<Cell> cellIterator = rowIterator.next().cellIterator();
                    while (cellIterator.hasNext()) {
                        Cell cell = cellIterator.next();
                        String cellString;
                        switch (cell.getCellType()) {
                            case STRING: cellString = cell.getStringCellValue(); break;
                            case NUMERIC: cellString = String.valueOf(cell.getNumericCellValue()); break;
                            default: cellString = "";
                        }
                        if (cellString.isBlank()) {
                            continue;
                        }
                        Boolean res = filters.stream().anyMatch(filter -> cellString.toUpperCase().contains(filter.toUpperCase()));
                        if (res) {
                            return true;
                        }
                    }
                }
            }
        } catch (Exception ex) {
            return false;
        }
        return false;
    }
}
