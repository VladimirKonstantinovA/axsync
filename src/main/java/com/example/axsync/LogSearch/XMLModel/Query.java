package com.example.axsync.LogSearch.XMLModel;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import java.util.ArrayList;

@XmlAccessorType(XmlAccessType.FIELD)
public class Query {

    @XmlElement(name = "SearchParams")
    private SearchParams searchParams;

    @XmlElementWrapper(name = "Filters")
    @XmlElement(name = "SearchString")
    private ArrayList<String> searchString;

    public SearchParams getSearchParams() {
        return searchParams;
    }

    public ArrayList<String> getFilters() {
        return searchString;
    }

    public Query() {
    }
}
