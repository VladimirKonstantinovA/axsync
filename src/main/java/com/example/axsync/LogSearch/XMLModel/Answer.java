package com.example.axsync.LogSearch.XMLModel;


import com.example.axsync.Enums.Status;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import java.util.ArrayList;

@XmlAccessorType(XmlAccessType.FIELD)
public class Answer {
    @XmlElement(name = "AnswerStatus")
    private Status status;

    @XmlElementWrapper(name = "AnswerResult")
    @XmlElement(name = "Result")
    private ArrayList<String> result;

    public Answer(Status status, ArrayList<String> result) {
        setAnswerStatus(status);
        setResult(result);
    }

    private void setAnswerStatus(Status answerStatus) {
        this.status = answerStatus;
    }

    private void setResult(ArrayList<String> result) {
        this.result = result;
    }
}
