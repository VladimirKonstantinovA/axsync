package com.example.axsync.LogSearch.XMLModel;

import com.example.axsync.Enums.SearchType;
import com.example.axsync.FileService.XML.XMLFileModelDataInterface;

import javax.xml.bind.annotation.*;

@XmlRootElement(name = "Root")
@XmlAccessorType(XmlAccessType.FIELD)
public class SearchFile implements XMLFileModelDataInterface {

    @XmlElement(name = "Identity")
    private String Identity;

    @XmlElement(name = "SearchType")
    private SearchType searchType;

    @XmlElement(name = "Query")
    private Query query;

    @XmlElement(name = "Answer")
    private Answer answer;

    public SearchType getSearchType() {
        return searchType;
    }

    public void setSearchType(SearchType searchType) {
        this.searchType = searchType;
    }

    public Query getQuery() {
        return query;
    }

    public void setQuery(Query query) {
        this.query = query;
    }

    public Answer getAnswer() {
        return answer;
    }

    public void setAnswer(Answer answer) {
        this.answer = answer;
    }

    public SearchFile() {
    }

}
