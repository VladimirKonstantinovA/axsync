package com.example.axsync.LogSearch.XMLModel;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import java.util.ArrayList;

@XmlAccessorType(XmlAccessType.FIELD)
public class SearchParams {
    @XmlElement(name = "LogPath")
    private String logPath;
    @XmlElement(name = "LogFileNameFilter")
    private ArrayList<String> logFileNameFilter;

    public String getLogPath() {
        return logPath;
    }

    public ArrayList<String> getLogFileNameFilter() {
        return logFileNameFilter;
    }

    public SearchParams() {}
}
