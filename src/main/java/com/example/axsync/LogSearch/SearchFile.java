package com.example.axsync.LogSearch;

import com.example.axsync.Enums.InfoMessage;
import com.example.axsync.Enums.Status;
import com.example.axsync.FileService.Model.AbstractFileData;
import com.example.axsync.FileService.FileSenderManager;
import com.example.axsync.LogSearch.XMLModel.Answer;
import com.example.axsync.LogSearch.XMLModel.SearchParams;
import com.example.axsync.FileService.XML.Tag;
import com.example.axsync.FileService.XML.TagPath;
import com.example.axsync.FileService.XML.XMLManager;

import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class SearchFile extends AbstractFileData {
    private com.example.axsync.LogSearch.XMLModel.SearchFile mappedData;


    public SearchFile(String fileName) {
        this.setInputFileName(fileName);
    }

    private Boolean isNodeCorrect() {
        String elementLOGPATH = XMLManager.getElement(this.getInputFileName(), TagPath.LOGPATH.getName(), Tag.LOGPATH);
        if (elementLOGPATH.isBlank()) {
            FileSenderManager.sendInvalidFile(InfoMessage.EMPTY_TAG_LOGPATH);
            return false;
        }

        String elementLOGFILENAMEFILTER = XMLManager.getElement(this.getInputFileName(), TagPath.LOGFILENAMEFILTER.getName(), Tag.LOGFILENAMEFILTER);
        if (elementLOGFILENAMEFILTER.isBlank()) {
            FileSenderManager.sendInvalidFile(InfoMessage.EMPTY_TAG_LOGFILENAMEFILTER);
            return false;
        }

        String elementSEARCHSTRING = XMLManager.getElement(this.getInputFileName(), TagPath.SEARCHSTRING.getName(), Tag.SEARCHSTRING);
        if (elementSEARCHSTRING.isBlank()) {
            FileSenderManager.sendInvalidFile(InfoMessage.EMPTY_TAG_SEARCHSTRING);
            return false;
        }

        return true;
    }

    @Override
    public void startProcess() {
        Boolean result = isNodeCorrect();
        if (!result) { return; }

        Object tmp = XMLManager.readFile(this, com.example.axsync.LogSearch.XMLModel.SearchFile.class);
        mappedData = (com.example.axsync.LogSearch.XMLModel.SearchFile) tmp;
        ArrayList<File> filesWithParams = getFiles(mappedData.getQuery().getSearchParams());
        ArrayList<File> filesWithFilteredContent = getFilteredFiles(filesWithParams, mappedData.getQuery().getFilters());

        SetAnswer(filesWithFilteredContent);
        XMLManager.writeFile(this, mappedData);

    }

    private void SetAnswer(ArrayList<File> data) {
       ArrayList<String> stringData = data.stream().map(file -> file.getPath()).collect(Collectors.toCollection((ArrayList::new)));
       mappedData.setAnswer(new Answer(Status.OK, stringData));
    }

    private ArrayList<File> getFiles(SearchParams params) {
        String path = params.getLogPath();
        ArrayList<String> filter = params.getLogFileNameFilter();
        Iterator<String> iterator = filter.iterator();

        ArrayList<File> findFiles = new ArrayList<>();
        while (iterator.hasNext()) {
            String fileName = iterator.next();
            Pattern pattern = Pattern.compile(fileName);

            FilenameFilter filenameFilter = (dir, name) -> {
                String fullPath = dir.toString() + "/" + name;
                File file = new File(fullPath);
                if (file.isDirectory()) {
                    return false;
                }

                Matcher matcher = pattern.matcher(fullPath);
                boolean math = matcher.find();
                return math;
            };

            File[] files = new File(path).listFiles(filenameFilter);
            Arrays.stream(files).forEach(file->findFiles.add(file));
        }
        return findFiles;
    }

    private ArrayList<File> getFilteredFiles(ArrayList<File> files, ArrayList<String> filters) {
        ArrayList<File> filesWithFilter = files.stream().filter(file-> FileContentManager.fileWithFilters(file, filters)).collect(Collectors.toCollection(ArrayList::new));
        return filesWithFilter;
    }

}
