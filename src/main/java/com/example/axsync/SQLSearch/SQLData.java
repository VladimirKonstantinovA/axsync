package com.example.axsync.SQLSearch;

import com.example.axsync.Enums.InfoMessage;
import com.example.axsync.Enums.Status;
import com.example.axsync.FileService.FileSenderManager;
import com.example.axsync.FileService.Model.AbstractFileData;
import com.example.axsync.FileService.XML.Tag;
import com.example.axsync.FileService.XML.TagPath;
import com.example.axsync.FileService.XML.XMLManager;
import com.example.axsync.SQLSearch.XMLModel.Answer;
import com.example.axsync.SQLSearch.XMLModel.Query;
import com.example.axsync.SQLSearch.XMLModel.ResultItem;
import com.example.axsync.SQLService.SQLManager;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.stream.Collectors;

public class SQLData extends AbstractFileData {
    private com.example.axsync.SQLSearch.XMLModel.SQLData mappedData;

    public SQLData(String fileName) {
        this.setInputFileName(fileName);
    }

    private Boolean isNodeCorrect() {
        String elementDBDRIVER = XMLManager.getElement(this.getInputFileName(), TagPath.DBDRIVER.getName(), Tag.DBDRIVER);
        if (elementDBDRIVER.isBlank()) {
            FileSenderManager.sendInvalidFile(InfoMessage.EMPTY_TAG_DBDRIVER);
            return false;
        }

        String elementDBPATH = XMLManager.getElement(this.getInputFileName(), TagPath.DBPATH.getName(), Tag.DBPATH);
        if (elementDBPATH.isBlank()) {
            FileSenderManager.sendInvalidFile(InfoMessage.EMPTY_TAG_DBPATH);
            return false;
        }

        String elementDBNAME = XMLManager.getElement(this.getInputFileName(), TagPath.DBNAME.getName(), Tag.DBNAME);
        if (elementDBNAME.isBlank()) {
            FileSenderManager.sendInvalidFile(InfoMessage.EMPTY_TAG_DBNAME);
            return false;
        }

        String elementDBUSER = XMLManager.getElement(this.getInputFileName(), TagPath.DBUSER.getName(), Tag.DBUSER);
        if (elementDBUSER.isBlank()) {
            FileSenderManager.sendInvalidFile(InfoMessage.EMPTY_TAG_DBUSER);
            return false;
        }

        String elementDBPASS = XMLManager.getElement(this.getInputFileName(), TagPath.DBPASS.getName(), Tag.DBPASS);
        if (elementDBPASS.isBlank()) {
            FileSenderManager.sendInvalidFile(InfoMessage.EMPTY_TAG_DBPASS);
            return false;
        }

        String elementQUERY = XMLManager.getElement(this.getInputFileName(), TagPath.QUERY.getName(), Tag.QUERY);
        if (elementQUERY.isBlank()) {
            FileSenderManager.sendInvalidFile(InfoMessage.EMPTY_TAG_QUERY);
            return false;
        }

        return true;
    }

    @Override
    public void startProcess() {
        Boolean result = isNodeCorrect();
        if (!result) { return; }

        Object tmp = XMLManager.readFile(this, com.example.axsync.SQLSearch.XMLModel.SQLData.class);
        mappedData = (com.example.axsync.SQLSearch.XMLModel.SQLData) tmp;

        Query query = mappedData.getQuery();
        HashMap<String, ArrayList> data = SQLManager.getSQLResult(query.getSearchParams(), query.getSearchString());
        if (data.size()==0) {
            return;
        }
        mappedData.setAnswer(new Answer(Status.OK, data.get("columns"), data.get("data")));
        XMLManager.writeFile(this, mappedData);

    }
}
