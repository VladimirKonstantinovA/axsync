package com.example.axsync.SQLSearch.XMLModel;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

@XmlAccessorType(XmlAccessType.FIELD)
public class DBParams {
    @XmlElement(name = "DBDriver")
    private String dbDriver;
    @XmlElement(name = "DBPath")
    private String dbPath;
    @XmlElement(name = "DBName")
    private String dbName;
    @XmlElement(name = "DBUser")
    private String dbUser;
    @XmlElement(name = "DBPass")
    private String dbPass;

    public String getDBDriver() {
        return dbDriver;
    }

    public String getDBPath() {
        return dbPath;
    }

    public String getDBName() {
        return dbName;
    }

    public String getDBUser() {
        return dbUser;
    }

    public String getDBPass() {
        return dbPass;
    }

    public DBParams() {}
}
