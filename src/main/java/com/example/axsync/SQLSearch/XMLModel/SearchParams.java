package com.example.axsync.SQLSearch.XMLModel;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

@XmlAccessorType(XmlAccessType.FIELD)
public class SearchParams {
    @XmlElement(name = "DBParams")
    private DBParams dbParams;
    @XmlElement(name = "QueryParams")
    private QueryParams queryParams;

    public DBParams getDBParams() {
        return dbParams;
    }

    public QueryParams getQueryParams() {
        return queryParams;
    }

    public SearchParams() {
    };

}