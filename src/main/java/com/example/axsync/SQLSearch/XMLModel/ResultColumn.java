package com.example.axsync.SQLSearch.XMLModel;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import java.util.ArrayList;

@XmlAccessorType(XmlAccessType.FIELD)
public class ResultColumn {
    private ArrayList<String> columns;

    public ResultColumn(ArrayList<String> columns) {
        this.columns = columns;
    }
}
