package com.example.axsync.SQLSearch.XMLModel;

import com.example.axsync.Enums.SearchType;
import com.example.axsync.FileService.XML.XMLFileModelDataInterface;

import javax.xml.bind.annotation.*;

@XmlRootElement(name = "Root")
@XmlAccessorType(XmlAccessType.FIELD)
public class SQLData implements XMLFileModelDataInterface {

    @XmlElement(name = "Identity")
    private String Identity;

    @XmlElement(name = "SearchType")
    private SearchType searchType;

    @XmlElement(name = "Query")
    private Query query;

    @XmlElement(name = "Answer")
    private Answer answer;

    public void setAnswer(Answer answer) {
        this.answer = answer;
    }

    public Query getQuery() {
        return query;
    }

    public SQLData() {}

}
