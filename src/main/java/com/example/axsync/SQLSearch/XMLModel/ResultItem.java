package com.example.axsync.SQLSearch.XMLModel;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import java.util.ArrayList;

@XmlAccessorType(XmlAccessType.FIELD)
public class ResultItem {
    @XmlElement(name = "Item")
    private ArrayList<String> item;

    public ResultItem(ArrayList<String> item) {
        this.item = item;
    }
}
