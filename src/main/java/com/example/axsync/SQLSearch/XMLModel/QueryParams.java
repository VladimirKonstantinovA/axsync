package com.example.axsync.SQLSearch.XMLModel;


import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

@XmlAccessorType(XmlAccessType.FIELD)
public class QueryParams {
    @XmlElement(name = "Query")
    private String query;
    @XmlElement(name = "Compare")
    private Compare compare;

    public String getQuery() {
        return query;
    }

    public Compare getCompare() {
        return compare;
    }

    public QueryParams() {}
}
