package com.example.axsync.SQLSearch.XMLModel;


import com.example.axsync.Enums.Status;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import java.util.ArrayList;

@XmlAccessorType(XmlAccessType.FIELD)
public class Answer {
    @XmlElement(name = "AnswerStatus")
    private Status status;

    @XmlElementWrapper(name = "Columns")
    @XmlElement(name = "Column")
    private ArrayList<String> columns;
    @XmlElementWrapper(name = "Results")
    @XmlElement(name = "Result")
    private ArrayList<ResultItem> result;

    public Answer(Status status, ArrayList<String> columns, ArrayList<ResultItem> result) {
        this.status = status;
        this.columns = columns;
        this.result = result;
    }

}
