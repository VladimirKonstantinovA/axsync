package com.example.axsync.SQLSearch.XMLModel;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import java.util.ArrayList;

@XmlAccessorType(XmlAccessType.FIELD)
public class Compare {
    @XmlElement(name = "Place")
    private String place;
    @XmlElement(name = "Type")
    private String type;
    @XmlElementWrapper(name = "Columns")
    @XmlElement(name = "Column")
    private ArrayList<String> columns;
    @XmlElementWrapper(name = "Values")
    @XmlElement(name = "Value")
    private ArrayList<String> values;

    public String getPlace() {
        return place;
    }

    public String getType() {
        return type;
    }

    public ArrayList<String> getColumns() {
        return columns;
    }

    public ArrayList<String> getValues() {
        return values;
    }

    public Compare() {}
}
