package com.example.axsync;

import com.example.axsync.Enums.InfoMessage;
import com.example.axsync.Enums.ReturnCode;
import com.example.axsync.FileService.InputFileManager;
import com.example.axsync.FileService.FileSenderManager;

public class mainClass {
    public static void start(String[] args) {
        //Check for args
        if (args == null && args.length == 0) {
            FileSenderManager.sendInvalidArgs(InfoMessage.EMPTY_ARGS);
            System.exit(ReturnCode.EMPTY_ARGS.ordinal());
        }
        if (! (args.length == 1)) {
            FileSenderManager.sendInvalidArgs(InfoMessage.INCORRECT_COUNT_ARGS);
            System.exit(ReturnCode.INCORRECT_COUNT_ARGS.ordinal());
        }
        if (! args[0].contains(".xml")) {
            FileSenderManager.sendInvalidArgs(InfoMessage.INVALID_ARGS);
            System.exit(ReturnCode.INVALID_ARGS.ordinal());
        }

        InputFileManager.processInputFile(args[0]);

        System.out.println("OK");
    }
}
