package com.example.axsync.SQLService;

import com.example.axsync.Enums.InfoMessage;
import com.example.axsync.FileService.FileSenderManager;
import com.example.axsync.SQLSearch.XMLModel.*;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

public class SQLManager {
    private static Connection getConnection(DBParams dbParams) {
        SQLDriver sqlDriver= SQLDriver.valueOf(dbParams.getDBDriver());
        if (sqlDriver == null) {
            FileSenderManager.sendInvalidFile(InfoMessage.INVALID_TAG_DBDRIVER);
        }

        Connection connection = SQLConnectionFactory.getConnection(sqlDriver, dbParams);
        return connection;
    }

    private static String completeQueryText(QueryParams queryParams) {
        String paramValuesTableName = "VTable";
        String paramValuesTableColumn = "vColumn";
        String query = queryParams.getQuery().toUpperCase();
        Compare compare = queryParams.getCompare();
        String type = compare.getType();
        String wherePlace = "WHERE";

        // Values table block
        String valuesTable = ", (VALUES ";
        String delimiter = "";
        Iterator<String> iteratorTable = compare.getValues().iterator();
        while (iteratorTable.hasNext()) {
            String data = iteratorTable.next();
            valuesTable = valuesTable.concat(delimiter).concat("('").concat(data).concat("')");
            delimiter = ",";
        }
        valuesTable = valuesTable.concat(") ").concat(paramValuesTableName).concat("(").concat(paramValuesTableColumn).concat(") ");
        query = query.replace(wherePlace, valuesTable + " " + "\n" + wherePlace + "\n");


        // WHERE block
        if (! compare.getPlace().isBlank() && ! compare.getType().isBlank()) {
            String condition = "";
            Iterator iterator = compare.getColumns().iterator();
            String multipleOperator = "";
            String symLIKE = "";
            String symAdd = "";
            if ("LIKE".equalsIgnoreCase(type)) {
                symLIKE = "'%'";
                symAdd = "+";
            }
            while (iterator.hasNext()) {
                String column = (String) iterator.next();
                condition = condition.concat(multipleOperator).concat(column).concat(" ").concat(type).concat(" ").concat("(").concat(symLIKE).concat(symAdd).concat(paramValuesTableName).concat(".").concat(paramValuesTableColumn).concat(symAdd).concat(symLIKE).concat(")");
                multipleOperator = " OR ";
            }
            String place = compare.getPlace();
            condition = place.concat("\n AND ( ").concat(condition).concat(" )");
            query = query.replace(place, condition);
        }

        return query;
    }

    public static HashMap<String, ArrayList> getSQLResult(SearchParams searchParams, ArrayList<String> searchString) {
        HashMap<String, ArrayList> returnData= new HashMap<>();
        Connection connection = getConnection(searchParams.getDBParams());
        String queryText = completeQueryText(searchParams.getQueryParams());
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(queryText);
            int columnCount = resultSet.getMetaData().getColumnCount();
            ResultSetMetaData metaData = resultSet.getMetaData();
            
            // Add columns name
            ArrayList<String> columnsData = new ArrayList<>(columnCount);
            for (int i = 1; i <= columnCount ; i++) {
                columnsData.add(metaData.getColumnName(i));
            }

            // Add data
            ArrayList<ResultItem> data = new ArrayList<>();
            while (resultSet.next()) {
                ArrayList<String> items = new ArrayList<>(columnCount);
                for (int i = 1; i <= columnCount ; i++) {
                    items.add(resultSet.getObject(i).toString());
                    if ("0E-16".equals(items.get(items.size()-1))) {
                        items.set((items.size()-1), "0");
                    }
                }
                // Filtering with out of query
                boolean isOk;
                if (searchString != null) {
                    isOk = items.stream().anyMatch(item -> searchString.stream().anyMatch(line -> item.toUpperCase().contains(line.toUpperCase())));
                }
                else { isOk = true; }
                if (isOk) {
                    ResultItem resultItem = new ResultItem(items);
                    data.add(resultItem);
                }
            }

            returnData.put("columns", columnsData);
            returnData.put("data", data);
        }
        catch (Exception ex) {
            FileSenderManager.sendInvalidFile(ex.getMessage());
        }
        return returnData;
    }
}
