package com.example.axsync.SQLService;

public enum SQLDriver {
    MSSQL("jdbc:sqlserver");

    final String name;

    SQLDriver(String name) {
        this.name = name;
    }

}
