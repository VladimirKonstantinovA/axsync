package com.example.axsync.SQLService;

import com.example.axsync.Enums.InfoMessage;
import com.example.axsync.FileService.FileSenderManager;
import com.example.axsync.SQLSearch.XMLModel.DBParams;

import java.sql.*;

public class SQLConnectionFactory {
    public static Connection getConnection(SQLDriver sqlDriver, DBParams dbParams) {
        Connection connObj = null;
        try {
            switch (sqlDriver) {
                case MSSQL: Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
                    String JDBC_URL = "jdbc:sqlserver://" + dbParams.getDBPath() + ";databaseName=" + dbParams.getDBName();
                    connObj = DriverManager.getConnection(JDBC_URL, dbParams.getDBUser(), dbParams.getDBPass());
                    break;
                default: FileSenderManager.sendInvalidFile(InfoMessage.INVALID_TAG_DBDRIVER); break;
            }
        }
        catch (Exception ex) {
            FileSenderManager.sendInvalidFile(ex.getMessage());
        }
        return connObj;
    }

}
