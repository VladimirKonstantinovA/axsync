package com.example.axsync.FileService.XML;

import com.example.axsync.FileService.Model.AbstractFileData;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.*;
import java.io.File;
import java.io.IOException;

public class XMLManager {
    public static <T> Object readFile(AbstractFileData owner, Class<T> dataClass) {
        try {
            JAXBContext context = JAXBContext.newInstance(dataClass);
            Unmarshaller unmarshaller = context.createUnmarshaller();
            Object tmp = unmarshaller.unmarshal(new File(owner.getInputFileName()));
            return tmp;

        } catch (JAXBException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public static <T extends XMLFileModelDataInterface> void writeFile(AbstractFileData owner, T  dataClass) {
        try {
            JAXBContext context = JAXBContext.newInstance(dataClass.getClass());
            Marshaller marshaller = context.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            marshaller.marshal(dataClass, new File(owner.getInputFileName()));

        } catch (JAXBException ex) {
            ex.printStackTrace();
        }
    }

    public static String getElement(String filePath, String tagPath, Tag element) {
        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        documentBuilderFactory.setNamespaceAware(true);
        DocumentBuilder documentBuilder;
        Document doc;
        try {
            documentBuilder = documentBuilderFactory.newDocumentBuilder();
            doc = documentBuilder.parse(filePath);
            XPathFactory xPathFactory = XPathFactory.newInstance();
            XPath xPath = xPathFactory.newXPath();
            XPathExpression expr = xPath.compile("/" + Tag.ROOTTAG.name + "/" + tagPath + "/" + element.name + "/text()");
            NodeList nodeList = (NodeList) expr.evaluate(doc, XPathConstants.NODESET);
            if (nodeList.getLength() == 0) {
                return "";
            }
            String value = nodeList.item(0).getNodeValue();
            return value;
        }
        catch (ParserConfigurationException | SAXException | XPathExpressionException | IOException ex) {
            ex.printStackTrace();
            return "";
        }
    }
}
