package com.example.axsync.FileService.XML;

public enum TagPath {
    LOGPATH("Query/SearchParams"),
    LOGFILENAMEFILTER("Query/SearchParams"),
    SEARCHSTRING("Query/Filters"),
    DBDRIVER("Query/SearchParams/DBParams"),
    DBPATH("Query/SearchParams/DBParams"),
    DBNAME("Query/SearchParams/DBParams"),
    DBUSER("Query/SearchParams/DBParams"),
    DBPASS("Query/SearchParams/DBParams"),
    QUERY("Query/SearchParams/QueryParams");

    final String name;

    TagPath(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
