package com.example.axsync.FileService.XML;

public enum Tag {
    IDENTITY("Identity"),
    SEARCHTYPE("SearchType"),
    LOGPATH("LogPath"),
    LOGFILENAMEFILTER("LogFileNameFilter"),
    SEARCHSTRING("SearchString"),
    ROOTTAG ("Root"),
    DBDRIVER("DBDriver"),
    DBPATH("DBPath"),
    DBNAME("DBName"),
    DBUSER("DBUser"),
    DBPASS("DBPass"),
    QUERY("Query");

    final String name;

    Tag(String name) {
        this.name = name;
    }
}
