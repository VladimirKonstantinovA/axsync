package com.example.axsync.FileService.Model;

public class AbstractFileData implements AbstractFileDataInterface {
    private String inputFileName;

    public String getInputFileName() {
        return inputFileName;
    }

    public void setInputFileName(String inputFileName) {
        this.inputFileName = inputFileName;
    }

    @Override
    public void startProcess() {

    }
}
