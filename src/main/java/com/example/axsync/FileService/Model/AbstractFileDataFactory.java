package com.example.axsync.FileService.Model;

import com.example.axsync.Enums.SearchType;
import com.example.axsync.LogSearch.SearchFile;
import com.example.axsync.SQLSearch.SQLData;

public class AbstractFileDataFactory {
    public static AbstractFileData getFileObject(SearchType searchType, String fileName) {
        AbstractFileData fileData = null;
        switch (searchType) {
            case UNKNOWN:
                break;
            case LOGSEARCH:
                fileData = new SearchFile(fileName);
                break;
            case SQLSEARCH:
                fileData = new SQLData(fileName);
                break;
        }
        return fileData;
    }
}
