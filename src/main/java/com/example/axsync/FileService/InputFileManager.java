package com.example.axsync.FileService;

import com.example.axsync.Enums.IdentityApp;
import com.example.axsync.Enums.InfoMessage;
import com.example.axsync.Enums.SearchType;
import com.example.axsync.FileService.Model.AbstractFileData;
import com.example.axsync.FileService.Model.AbstractFileDataFactory;
import com.example.axsync.FileService.XML.Tag;
import com.example.axsync.FileService.XML.XMLManager;

public class InputFileManager {

    private static Boolean isFileCorrect(String fileName) {
        String element = XMLManager.getElement(fileName, "", Tag.IDENTITY);
        if (element.isBlank()) {
            return false;
        }
        return  (IdentityApp.AX_1C_Syncronization.toString().equals(element));
    }

    private static SearchType getSearchType(String fileName) {
        String element = XMLManager.getElement(fileName, "", Tag.SEARCHTYPE);
        if (element.isEmpty()) {
            return SearchType.UNKNOWN;
        }
        return SearchType.valueOf(element.toUpperCase());
    }

    public static void processInputFile(String fileName){
        if (! isFileCorrect(fileName)) {
            FileSenderManager.sendInvalidFile(InfoMessage.INVALID_TAG_IDENTITY);
            return;
        }

        SearchType searchType = (getSearchType(fileName));
        AbstractFileData fileData = AbstractFileDataFactory.getFileObject(searchType, fileName);
        if (fileData == null) {
            FileSenderManager.sendInvalidFile(InfoMessage.INVALID_TAG_SEARCHTYPE);
            return;
        }
        fileData.startProcess();
    }

}
